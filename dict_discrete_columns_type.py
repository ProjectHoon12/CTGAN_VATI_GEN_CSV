BCTC3_CDKT_A_1 = [
    # PROPERTIES 00
    # Discrete Columns
    ['STT',
     'CHỈ TIÊU',
     'Thuyết minh',
     'Số cuối năm',
     'Số đầu năm'],
    
    # PROPERTIES 01
    # Projected row header
    [0],

    # PROPERTIES 02
    # Spanning cell
    # Row index in bold data
    [0],

    # PROPERTIES 03
    # Cell index in row have spanning cell
    [[0, 1]],

    # PROPERTIES 04
    # Content of spc
    [['PROJECT HOON', 'TEST']],

    # PROPERTIES 05
    # Alignment of text in spanning cell
    [['Center', 'Middle']]
]

DICT_DISCRETE_COLUMNS_TYPE = {
    'BCTC3_CDKT_A_1': BCTC3_CDKT_A_1
}
